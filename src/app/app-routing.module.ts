import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CalendarioComponent } from '../app/calendario/calendario.component';
import { AuthGuard } from './auth.guard';
import { MapComponent } from './map/map.component';

const routes: Routes = [
  {
    path: '',
    loadChildren: './portafolio/portafolio.module#PortafolioModule'
  },
  {
    path: 'calendario',
    component: CalendarioComponent
  },
  {
    path: 'login',
    loadChildren: './login/login.module#LoginModule'
  },
  {
    path: 'create',
    loadChildren: './create-user/create-user.module#CreateUserModule'
  },
  {
    path: 'reset-password',
    loadChildren: './reset-password/reset-password.module#ResetPasswordModule'
  },
  {
    path: 'dashboard',
    canActivate: [AuthGuard],
    loadChildren: './dashboard/dashboard.module#DashboardModule'
  },
  {
    path: 'maps',
    component: MapComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
