import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AppConfig } from 'src/app.config';
import { AlertService } from '../alerts/alert.service';
import { LoginService } from '../login/login.service';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss'],
  providers: [AlertService, LoginService]
})
export class CreateUserComponent implements OnInit {

  createForm: FormGroup
  private usersList: any[];

  constructor(
    private fb: FormBuilder,
    private alertService: AlertService,
    private loginService: LoginService

  ) { }

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.createForm = this.fb.group({
      name: [''],
      firstLastName: [''],
      secondLastName: [''],
      userName: [''],
      password: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(45), Validators.pattern(AppConfig.PASSWORD_PATTERN)]],
      passwordRepeat: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(45), Validators.pattern(AppConfig.PASSWORD_PATTERN)]],
      email: ['', [Validators.required, Validators.pattern(AppConfig.EMAIL_PATTERN), Validators.maxLength(45)]],
      phoneNumber: ['', [Validators.required, Validators.pattern("^[0-9]*$"), Validators.minLength(8)]],
      birthDay: [''],
      sex: ['']
    });
  }

  create() {
    let userToCreate = this.createForm.value;
    this.loginService.createUser(userToCreate).subscribe(
      data => this.handleUser(data),
      error => this.handleError(error)
    )
  }

  handleUser(data) {
    this.alertService.openSnackBar('Usuario Creado', 'success');
  }

  handleError(error) {
    this.alertService.openSnackBar('Usuario Creado', 'error');
  }
}
