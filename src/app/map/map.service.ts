import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
const headers = new HttpHeaders({ 'Content-Type': 'application/json' });

const urlApi = 'http://localhost:3000/routes-api/';


@Injectable({
  providedIn: 'root'
})
export class MapService {

  constructor(
    private httpClient: HttpClient
  ) {

  }

  getRoutes(): Observable<any[]> {
    const url = urlApi + 'Routes';
    return this.httpClient.get<any[]>(url, httpOptions);
  }

  getPointsByRouteId(id: number): Observable<any> {
    const url = urlApi + 'Points';
    const filter = { where: { routefk: id } };
    const params = new HttpParams().set('filter', JSON.stringify(filter));
    const options = { headers, params };
    return this.httpClient.get<any>(url, options);
  }

  postRoute(route): Observable<any> {
    const url = urlApi + 'Routes';
    return this.httpClient.post<any[]>(url, route, httpOptions);
  }

  postPoint(point): Observable<any> {
    const url = urlApi + 'Points';
    return this.httpClient.post<any[]>(url, point, httpOptions);
  }

}
