
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import {
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatRadioModule,
    MatListModule,
    MatToolbarModule,
    MatSidenavModule,
    MatIconModule,
    MatCardModule,
    MatSnackBarModule,
    MatSelectModule,
    MatChipsModule,
    MatCheckboxModule,
    MatPaginatorModule,
    MatTableModule,
    MatMenuModule,
    MatDialogModule,
    MatTabsModule,
    MatTooltipModule,
    MatAutocompleteModule,
    MatSlideToggleModule,
    MatExpansionModule,
    MatStepperModule,
    MatSortModule,
    MatDatepickerModule,
    MatNativeDateModule
} from '@angular/material';
import { OverlayModule } from '@angular/cdk/overlay';


@NgModule({
    exports: [
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatRadioModule,
        MatRippleModule,
        MatListModule,
        MatToolbarModule,
        MatSidenavModule,
        MatIconModule,
        MatCardModule,
        OverlayModule,
        MatSnackBarModule,
        MatSelectModule,
        MatChipsModule,
        MatCheckboxModule,
        MatPaginatorModule,
        MatTableModule,
        MatMenuModule,
        MatDialogModule,
        MatTabsModule,
        MatTooltipModule,
        MatAutocompleteModule,
        MatExpansionModule,
        MatSlideToggleModule,
        MatStepperModule,
        MatSortModule,
        MatDatepickerModule,
        MatNativeDateModule
    ],
    declarations: []
})
export class MaterialModule { }
