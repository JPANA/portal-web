import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlertService } from '../alerts/alert.service';
import { AppConfig } from 'src/app.config';
import { LoginService } from '../login/login.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss'],
  providers: [AlertService, LoginService]
})
export class ResetPasswordComponent implements OnInit {
  public restoreForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private loginService: LoginService,
    private alertService: AlertService
  ) { }


  ngOnInit() {
    this.buildForm();
  }


  buildForm() {
    this.restoreForm = this.fb.group({
      email: ['', [Validators.required, Validators.pattern(AppConfig.EMAIL_PATTERN), Validators.maxLength(45)]]
    });
  }

  restore() {
    this.loginService.updatePassword(this.restoreForm.value).subscribe(
      data => this.handleRestore(data),
      error => this.handleError(error)
    )
    //this.alertService.openSnackBar('Se ha enviado una contraseña temporal, revise su correo electrónico.', 'success');
  }

  handleRestore(data) {
    this.alertService.openSnackBar(data.message, 'success');
  }

  handleError(data) {
    this.alertService.openSnackBar('Ah ocurrido un error.', 'error');
  }

}
