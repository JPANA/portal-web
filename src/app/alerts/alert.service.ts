import { Injectable, Inject } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';



@Injectable()
export class AlertService {

    constructor(
        private snackBar: MatSnackBar
    ) { }

    public openSnackBar(message: string, type: string) {
        const config = new MatSnackBarConfig();
        if (type === 'error') {
            config.panelClass = ['error-class'];
        } else if (type === 'success') {
            config.panelClass = ['success-class'];
        } else if (type === 'warning') {
            config.panelClass = ['warning-class'];
        }
        config.duration = 5000;
        config.verticalPosition = 'top';
        return this.snackBar.open(message, null, config);
    }

}
