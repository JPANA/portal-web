import { Component, OnInit, AfterContentInit, AfterViewInit } from '@angular/core';
import * as L from 'leaflet';
import 'leaflet-routing-machine';
import 'leaflet.awesome-markers';

import { MapService } from './map.service';
import { AlertService } from '../alerts/alert.service';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
  providers: [MapService]
})
export class MapComponent implements OnInit, AfterViewInit {
  private map;
  public routesList: any;
  public pointsList: any;
  public createRouteFlag = false;
  public routesListToCreate = [];
  public routeName: string;
  public createError = false;

  constructor(
    private mapService: MapService,
    private alertService: AlertService
  ) {
    this.routesList = [];
    this.pointsList = [];
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.initMap();
  }

  private initMap() {
    this.map = L.map('map', {
      center: [9.8545474, -83.9090366],
      zoom: 16
    });
    L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
      attribution: '© <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(this.map);

    L.control.scale().addTo(this.map);
    this.loadRoutes();
  }

  loadPoints() {
    let lat;
    let lon;

    var start = L.icon({
      iconUrl: './assets/images/start.png',
      iconAnchor: [22, 30],
      popupAnchor: [-3, -76]
    });

    var finish = L.icon({
      iconUrl: './assets/images/finish.png',
      iconAnchor: [22, 30],
      popupAnchor: [-3, -76]
    });

    var point = L.icon({
      iconUrl: './assets/images/point.png',
      iconAnchor: [22, 30],
      popupAnchor: [-3, -76]
    });

    let lastPoint = this.pointsList[this.pointsList.length - 1];
    let firstPoint = this.pointsList[0];

    for (let item of this.pointsList) {
      if (item === firstPoint) {
        L.marker([firstPoint.latitude, firstPoint.longitude], {
          icon: start
        }).addTo(this.map);
      } else {
        if (item === lastPoint) {
          L.marker([lastPoint.latitude, lastPoint.longitude], {
            icon: finish
          }).addTo(this.map);
        } else {
          L.marker([item.latitude, item.longitude], {
            icon: point
          }).addTo(this.map);
        }
      }
    }

    for (let item of this.pointsList) {
      L.Routing.control({
        waypoints: [
          L.latLng(lat, lon),
          L.latLng(item.latitude, item.longitude)
        ],
        createMarker: function () {
          return null;
        },
        routeWhileDragging: false
      }).addTo(this.map);
      lat = item.latitude;
      lon = item.longitude;
    }
  }

  loadRoutes() {
    this.mapService.getRoutes().subscribe(
      data => {
        this.routesList = data;
      }
    )
  }

  loadPointsByPointId(id) {
    this.mapService.getPointsByRouteId(id).subscribe(
      data => {
        this.map.remove();
        this.ngAfterViewInit();
        this.pointsList = data;
        this.loadPoints();
      }
    )
  }

  selectPointOnMap(array) {
    this.map.on('click',
      function (e) {
        var coord = e.latlng.toString().split(',');
        var lat = coord[0].split('(');
        var lng = coord[1].split(')');
        let point = {
          "pointpk": 0,
          "latitude": lat[1],
          "longitude": lng[0],
          "description": "Punto en el mapa",
          "routefk": 0
        }
        array.push(point);
      });
  }

  createRoute() {
    this.createRouteFlag = true;
    this.map.remove();
    this.ngAfterViewInit();
    this.selectPointOnMap(this.routesListToCreate);
  }

  saveRoute() {
    if (this.routeName == "" || this.routeName == undefined || this.routesListToCreate.length < 2) {
      this.alertService.openSnackBar('Error al crear la ruta, revise los datos.', 'error');
    } else {
      let route = {
        "routespk": 0,
        "name": this.routeName
      }

      this.mapService.postRoute(route).subscribe(
        route => {
          let routeFk = route.routespk;
          for (const point of this.routesListToCreate) {
            point.routefk = routeFk;
            this.mapService.postPoint(point).subscribe(
              response => {
                this.alertService.openSnackBar('Ruta creada', 'success');
                this.loadRoutes();
                this.createRouteFlag = false;
                this.clearData();
              }
            )
          }
        })
    }

  }

  clearData() {
    this.routesListToCreate = [];
    this.routeName = '';
  }

}
