import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { AlertService } from '../alerts/alert.service';
import { Router } from '@angular/router';

import { LoginService } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [AlertService, LoginService]
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup;
  private usersList: any

  constructor(
    private fb: FormBuilder,
    private alertService: AlertService,
    private router: Router,
    private loginService: LoginService
  ) {
    this.usersList = [{
      name: "pana",
      firstLastName: "pana",
      secondLastName: "pana",
      userName: "pana",
      password: "Pana1806",
      passwordRepeat: "Pana1806",
      email: "jose.gm1806@gmail.com",
      phoneNumber: "12121212",
      birthDay: "2018-12-06"
    }];
  }

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.loginForm = this.fb.group({
      userName: [''],
      password: ['', Validators.compose([Validators.required, Validators.minLength(0)])]
    });
  }

  login() {
    let userToLogin = this.loginForm.value;
    this.loginService.getLogin(userToLogin).subscribe(
      data => this.handleLogin(data),
      error => this.handleError(error)
    )
  }

  handleLogin(data) {
    if (data.userInfo.sex == '1') {
      this.alertService.openSnackBar('Bienvenido.', 'success');
    } else {
      this.alertService.openSnackBar('Bienvenida.', 'success');
    }
    localStorage.setItem('logged', 'true');
    localStorage.setItem('userName', data.userInfo.name + ' ' + data.userInfo.firstLastName + ' ' + data.userInfo.secondLastName);
    localStorage.setItem('userNameInitials', data.userInfo.name[0] + data.userInfo.firstLastName[0]);
    this.router.navigate(['dashboard']);
  }

  handleError(error) {
    this.alertService.openSnackBar('Correo o contraseña incorrectos.', 'error');
  }

}
