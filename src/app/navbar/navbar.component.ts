import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertService } from '../alerts/alert.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  public userName: string;
  public userNameInitials: string;

  constructor(
    private route: Router,
    private alertService: AlertService
  ) { }

  ngOnInit() {
    this.userNameInitials = localStorage.getItem("userNameInitials");
    this.userName = localStorage.getItem("userName");
  }

  logOut() {
    localStorage.removeItem('logged');
    localStorage.removeItem('userNameInitials');
    localStorage.removeItem('userName');
    this.route.navigate(['/login']);
    this.alertService.openSnackBar('Sesión cerrada.', 'warning');
  }

}
