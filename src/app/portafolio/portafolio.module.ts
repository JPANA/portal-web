import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbCollapseModule } from '@ng-bootstrap/ng-bootstrap';

import { PortafolioComponent } from './portafolio.component'
import { PortafolioRoutingModule } from './portafolio-routing.module'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MaterialModule } from '../material/material.module';


@NgModule({
  declarations: [
    PortafolioComponent
  ],
  imports: [
    CommonModule,
    PortafolioRoutingModule,
    NgbCollapseModule,
    NgbModule,
    MaterialModule
  ],
  providers: [],
  bootstrap: []
})
export class PortafolioModule { }
