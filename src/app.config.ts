export class AppConfig {
  public static EMAIL_PATTERN = '[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}';
  public static PASSWORD_PATTERN = `((?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,45})`;
  public static ERROR_STATE = -1;
  public static CHANGED_EMAIL = 1;
  public static UNCHANGED_EMAIL = 0;
}