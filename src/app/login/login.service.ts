import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable()

export class LoginService {
    constructor(
        private hhttp: HttpClient
    ) { }

    url = 'http://localhost/API_LOGIN/';

    getLogin(loginData): Observable<any> {
        const url = this.url + 'login.php';
        return this.hhttp.post<any>(url, loginData, httpOptions);
    }

    createUser(createUserData): Observable<any> {
        const url = this.url + 'createUser.php';
        return this.hhttp.post<any>(url, createUserData, httpOptions);
    }

    updatePassword(updatePassword): Observable<any> {
        const url = this.url + 'changePassword.php';
        return this.hhttp.post<any>(url, updatePassword, httpOptions);
    }
}