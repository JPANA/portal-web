import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-portafolio',
  templateUrl: './portafolio.component.html',
  styleUrls: ['./portafolio.component.scss']
})
export class PortafolioComponent {
  public isCollapsed = false;

  changeShow() {
    console.log(this.isCollapsed);
    this.isCollapsed = !this.isCollapsed;
  }

}
