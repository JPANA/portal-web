import { Component, OnInit, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-calendario',
  templateUrl: './calendario.component.html',
  styleUrls: ['./calendario.component.scss']
})
export class CalendarioComponent implements OnInit {
  public meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
  public lasemana = ["Domigo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"];
  public diasSemana = ["Lun", "Mar", "Mié", "Jue", "Vie", "Sáb", "Dom"];

  public hoy;
  public diaSemHoy;
  public diaHoy;
  public mesHoy;
  public annoHoy;

  public tit;
  public ant;
  public pos;

  public f0;

  public pie;

  public buscar;

  public mesCal;
  public annoCal;

  public primeroMes;

  constructor(@Inject(DOCUMENT) document) { }

  ngOnInit() {
    this.hoy = new Date();
    this.diaSemHoy = this.hoy.getDay();
    this.diaHoy = this.hoy.getDate();
    this.mesHoy = this.hoy.getMonth();
    this.annoHoy = this.hoy.getFullYear();

    this.tit = document.getElementById("titulos");
    this.ant = document.getElementById("anterior");
    this.pos = document.getElementById("posterior");

    this.f0 = document.getElementById("fila0");

    this.pie = document.getElementById("fechaactual")
    this.buscar = document.getElementById("buscar");

    this.pie.innerHTML = this.lasemana[this.diaSemHoy] + " , " + this.diaHoy + " de " + this.meses[this.mesHoy] + " de " + this.annoHoy + ".";
    this.pie.style.backgroundColor = "#DADFE3";
    this.buscar.buscaanno.value = this.annoHoy;

    this.mesCal = this.mesHoy;
    this.annoCal = this.annoHoy;

    this.cabecera();
    this.primeraLinea();
    this.escribirDias();
  }

  cabecera() {
    this.tit.innerHTML = this.meses[this.mesCal] + " del " + this.annoCal;
    let mesAnt = this.mesCal - 1;
    let mesPos = this.mesCal + 1;
    if (mesAnt < 0) {
      mesAnt = 11;
    }
    if (mesPos > 11) {
      mesPos = 0
    }
    this.ant.innerHTML = "« " + this.meses[mesAnt];
    this.pos.innerHTML = this.meses[mesPos] + " »";
  }

  primeraLinea() {
    for (let i = 0; i < 7; i++) {
      let celda0 = document.getElementsByTagName("th")[i];
      celda0.innerHTML = this.diasSemana[i];
    }
  }

  escribirDias() {
    this.primeroMes = new Date(this.annoCal, this.mesCal, 1);
    let prSem = this.primeroMes.getDay();
    prSem--;
    if (prSem == -1) {
      prSem = 6;
    }

    let diaPrMes = this.primeroMes.getDate();
    let prCelda = diaPrMes - prSem;

    let empezar = this.primeroMes.setDate(prCelda);
    let diaMes = new Date();
    diaMes.setTime(empezar);

    for (let i = 1; i < 7; i++) {
      let fila = document.getElementById("fila" + i);
      for (let j = 0; j < 7; j++) {
        let miDia = diaMes.getDate();
        let miMes = diaMes.getMonth();
        let miAnno = diaMes.getFullYear();
        let celda = fila.getElementsByTagName("td")[j];
        celda.innerHTML = miDia.toString();

        celda.style.backgroundColor = "#191919";
        celda.style.color = "#56B885";


        if (j == 6) {
          celda.style.color = "#aa2226";
        }

        if (miMes != this.mesCal) {
          celda.style.color = "#a0babc";
        }

        if (miMes == this.mesHoy && miDia == this.diaHoy && miAnno == this.annoHoy) {
          celda.style.backgroundColor = "rgba(0,180,147, 0.1)";
          celda.innerHTML = "<cite title= 'Fecha Actual'>" + miDia + "</cite>";
        }

        miDia = miDia + 1;
        diaMes.setDate(miDia);
      }

    }

  }

  mesAntes() {
    let nuevoMes = new Date();
    this.primeroMes--;
    nuevoMes.setTime(this.primeroMes);
    this.mesCal = nuevoMes.getMonth();
    this.annoCal = nuevoMes.getFullYear();
    this.cabecera();
    this.escribirDias();
  }

  mesDespues() {
    let nuevoMes = new Date();
    this.mesCal = this.hoy.getMonth();
    let tiempoUnix = this.primeroMes.getTime();
    tiempoUnix = tiempoUnix + (45 * 24 * 60 * 60 * 1000);
    nuevoMes.setTime(tiempoUnix);
    this.mesCal = nuevoMes.getMonth();
    this.annoCal = nuevoMes.getFullYear();
    this.cabecera();
    this.escribirDias();
  }

  actualizar() {
    this.mesCal = this.hoy.getMonth();
    this.annoCal = this.hoy.getFullYear();
    this.cabecera();
    this.escribirDias();
  }

  miFecha() {
    let miAnno = this.buscar.buscaanno.value;
    let listaMeses = this.buscar.buscames;
    let opciones = listaMeses.options;
    let num = listaMeses.selectedIndex;
    let miMes = opciones[num].value;

    if (isNaN(miAnno) || miAnno < 1) {
      alert("El año NO es válido: \n Debe ser un número mayor que 0")
    }
    else {
      let miFe = new Date();
      miFe.setMonth(miMes);
      miFe.setFullYear(miAnno);
      this.mesCal = miFe.getMonth();
      this.annoCal = miFe.getFullYear();
      this.cabecera();
      this.escribirDias();
    }
  }


}
